class ModelError {
  ModelError({
    this.cod,
    this.message,
  });

  String? cod;
  String? message;

  factory ModelError.fromJson(Map<String, dynamic> json) => ModelError(
        cod: json["cod"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "cod": cod,
        "message": message,
      };
}
