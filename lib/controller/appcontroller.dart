import 'package:get/get.dart';
import 'package:pretest_jds/data/api/api_service/api_service.dart';
import 'package:pretest_jds/model/model_city.dart';
import 'package:pretest_jds/model/model_province.dart';
import 'package:pretest_jds/model/model_weather_forecast.dart';
import 'package:pretest_jds/model/model_weather_today.dart';

class AppController extends GetxController {
  final now = DateTime.now();
  var errorMessage = ''.obs;

  //Province Variable
  var isLoadingProvince = false.obs;
  var listProvince = <Province>[].obs;
  var provinceValue = ''.obs;
  var successGetProvince = false.obs;

  //City Variable
  var isLoadingCity = false.obs;
  var listCity = <Regency>[].obs;
  var cityValue = ''.obs;
  var successGetCity = false.obs;

  //Weather Today Variable
  var isLoadingWeatherToday = false.obs;
  ModelWeatherToday? weatherToday;
  var listIconToday = <WeatherToday>[].obs;
  var successGetWeatherToday = false.obs;

  //Weather Forecast Variable
  var isLoadingForecast = false.obs;
  var successGetWeatherForecast = false.obs;
  var listWeatherForecastToday = <ListElement>[].obs;
  var listWeatherForecastDay1 = <ListElement>[].obs;
  var listWeatherForecastDay2 = <ListElement>[].obs;
  var listWeatherForecastDay3 = <ListElement>[].obs;
  var listWeatherForecastDay4 = <ListElement>[].obs;
  var listWeatherForecastDay5 = <ListElement>[].obs;

  var listWeatherDetailForecastToday = <Weather>[].obs;
  var listWeatherDetailForecastD1 = <Weather>[].obs;
  var listWeatherDetailForecastD2 = <Weather>[].obs;
  var listWeatherDetailForecastD3 = <Weather>[].obs;
  var listWeatherDetailForecastD4 = <Weather>[].obs;
  var listWeatherDetailForecastD5 = <Weather>[].obs;

  setProvince(String value) {
    provinceValue.value = value;
  }

  setCity(String value) {
    cityValue.value = value;
  }

  getProvince() async {
    try {
      isLoadingProvince.value = true;
      final result = await ApiService().getApiProvince();
      if (result != null) {
        result.rajaongkir!.results!.forEach((element) {
          listProvince.add(element);
        });
        successGetProvince.value = true;
      } else {
        print('Failed');
      }
    } catch (e) {
      isLoadingProvince.value = false;
      errorMessage.value = e.toString();
      print(errorMessage);
    } finally {
      isLoadingProvince.value = false;
    }
  }

  getCity() async {
    try {
      isLoadingCity.value = true;
      final result = await ApiService().getApiCity(provinceValue.value);
      if (result != null) {
        result.rajaongkir!.results!
            .where((element) => element.type == 'Kota')
            .forEach((element) {
          listCity.add(element);
        });
        successGetCity.value = true;
      } else {
        print('Failed');
      }
    } catch (e) {
      isLoadingCity.value = false;
      errorMessage.value = e.toString();
      print(errorMessage);
    } finally {
      isLoadingCity.value = false;
    }
  }

  getWeatherToday() async {
    try {
      weatherToday = await ApiService()
          .getApiWeatherToday(cityValue.value); //cityValue.value);
      if (weatherToday != null) {
        weatherToday!.weather!.forEach((element) {
          listIconToday.add(element);
        });
        successGetWeatherToday.value = true;
      } else {
        print('Failed');
      }
    } catch (e) {
      isLoadingWeatherToday.value = false;
      errorMessage.value = e.toString();
      print(errorMessage);
    } finally {
      isLoadingWeatherToday.value = false;
    }
  }

  getWeatherForecast(/*String city*/) async {
    final today = DateTime(now.year, now.month, now.day);
    final dayOne = DateTime(now.year, now.month, now.day + 1);
    final dayTwo = DateTime(now.year, now.month, now.day + 2);
    final dayThree = DateTime(now.year, now.month, now.day + 3);
    final dayFour = DateTime(now.year, now.month, now.day + 4);
    final dayFive = DateTime(now.year, now.month, now.day + 5);
    //
    try {
      final result = await ApiService().getApiWeatherForecast(cityValue.value);
      if (result != null) {
        result.list!
            .where((element) => element.dtTxt!.day == today.day)
            .forEach((element) {
          listWeatherForecastToday.add(element);
        });

        //
        result.list!
            .where((element) => element.dtTxt!.day == dayOne.day)
            .forEach((element) {
          listWeatherForecastDay1.add(element);
        });
        result.list!
            .where((element) => element.dtTxt!.day == dayTwo.day)
            .forEach((element) {
          listWeatherForecastDay2.add(element);
        });
        result.list!
            .where((element) => element.dtTxt!.day == dayThree.day)
            .forEach((element) {
          listWeatherForecastDay3.add(element);
        });
        result.list!
            .where((element) => element.dtTxt!.day == dayFour.day)
            .forEach((element) {
          listWeatherForecastDay4.add(element);
        });
        result.list!
            .where((element) => element.dtTxt!.day == dayFive.day)
            .forEach((element) {
          listWeatherForecastDay5.add(element);
        });
        //
        //
        listWeatherForecastToday.forEach((x) {
          x.weather!.forEach((y) {
            listWeatherDetailForecastToday.add(y);
          });
        });
        listWeatherForecastDay1.forEach((x) {
          x.weather!.forEach((y) {
            listWeatherDetailForecastD1.add(y);
          });
        });
        listWeatherForecastDay2.forEach((x) {
          x.weather!.forEach((y) {
            listWeatherDetailForecastD2.add(y);
          });
        });
        listWeatherForecastDay3.forEach((x) {
          x.weather!.forEach((y) {
            listWeatherDetailForecastD3.add(y);
          });
        });
        listWeatherForecastDay4.forEach((x) {
          x.weather!.forEach((y) {
            listWeatherDetailForecastD4.add(y);
          });
        });
        listWeatherForecastDay5.forEach((x) {
          x.weather!.forEach((y) {
            listWeatherDetailForecastD5.add(y);
          });
        });
        successGetWeatherForecast.value = true;
      } else {
        print('Failed');
      }
    } catch (e) {
      isLoadingForecast.value = false;
      errorMessage.value = e.toString();
      print(errorMessage);
    } finally {
      isLoadingForecast.value = false;
    }
  }

  void clear() {
    // listProvince.clear();
    // listCity.clear();
    //
    listWeatherForecastToday.clear();
    listIconToday.clear();
    listWeatherDetailForecastToday.clear();
    successGetWeatherToday.value = false;
    //
    successGetWeatherForecast.value = false;
    listWeatherForecastDay1.clear();
    listWeatherForecastDay2.clear();
    listWeatherForecastDay3.clear();
    listWeatherForecastDay4.clear();
    listWeatherForecastDay5.clear();
    //
    listWeatherDetailForecastD1.clear();
    listWeatherDetailForecastD2.clear();
    listWeatherDetailForecastD3.clear();
    listWeatherDetailForecastD4.clear();
    listWeatherDetailForecastD5.clear();
    //
    errorMessage.value = '';
  }
}
