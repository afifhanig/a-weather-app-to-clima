import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'dart:io';
import 'package:pretest_jds/model/model_province.dart';
import 'package:pretest_jds/model/model_city.dart';
import 'package:pretest_jds/data/api/constant/endpoint.dart';
import 'package:pretest_jds/model/model_weather_forecast.dart';
import 'package:pretest_jds/model/model_weather_today.dart';

class ApiService {
  Map<String, String> requestHeaders = {
    'Content-type': 'application/json',
    'Accept': 'application/json',
    'key': '4556007511116b607443a00ec61a4d46'
  };

  Future<ModelProvince?> getApiProvince() async {
    try {
      ModelProvince modelProvince;
      var url = Uri.http(Endpoints.endpointUrlLocation, '/starter/province');
      print(url);
      var response = await http.get(url, headers: requestHeaders);
      if (response.statusCode == 200) {
        var jsonResponse =
            convert.jsonDecode(response.body) as Map<String, dynamic>;
        modelProvince = ModelProvince.fromJson(jsonResponse);
        print('Request successful: $jsonResponse');
        return modelProvince;
      } else {
        throw 'Request failed with status: ${response.statusCode}';
      }
    } on SocketException {
      throw 'No internet connection 😑';
    } on HttpException {
      throw 'Couldn\'t find the post 😱';
    } on FormatException {
      throw 'Bad response format 👎';
    }
  }

  Future<ModelCity?> getApiCity(String province) async {
    try {
      ModelCity modelKota;
      var url = Uri.http(Endpoints.endpointUrlLocation, '/starter/city',
          {'province': province});
      print(url);
      var response = await http.get(url, headers: requestHeaders);
      if (response.statusCode == 200) {
        var jsonResponse =
            convert.jsonDecode(response.body) as Map<String, dynamic>;
        print(jsonResponse);
        modelKota = ModelCity.fromJson(jsonResponse);
        print('Request successful: $jsonResponse');
        return modelKota;
      } else {
        throw 'Request failed with status: ${response.statusCode}';
      }
    } on SocketException {
      throw 'No internet connection 😑';
    } on HttpException {
      throw 'Couldn\'t find the post 😱';
    } on FormatException {
      throw 'Bad response format 👎';
    }
  }

  Future<ModelWeatherToday?> getApiWeatherToday(String city) async {
    try {
      ModelWeatherToday? modelWeatherToday;
      var url = Uri.http(Endpoints.endpointUrlWeather, '/data/2.5/weather', {
        'q': city,
        'appid': Endpoints.appIdWeather,
        'units': 'metric',
      });
      print(url);
      var response = await http.get(url);
      if (response.statusCode == 200) {
        var jsonResponse =
            convert.jsonDecode(response.body) as Map<String, dynamic>;
        modelWeatherToday = ModelWeatherToday.fromJson(jsonResponse);
        print('Request successful: $jsonResponse');
        return modelWeatherToday;
      } else if (response.statusCode == 404) {
        throw 'City does not found, we\'ll update it soon';
      } else
        throw 'Request failed with status: ${response.statusCode}';
    } on SocketException {
      throw 'No internet connection 😑';
    } on HttpException {
      throw 'Couldn\'t find the post 😱';
    } on FormatException {
      throw 'Bad response format 👎';
    }
  }

  Future<ModelWeatherForecast?> getApiWeatherForecast(String city) async {
    try {
      ModelWeatherForecast modelWeatherForecast;
      var url = Uri.http(Endpoints.endpointUrlWeather, '/data/2.5/forecast', {
        'q': city,
        'appid': Endpoints.appIdWeather,
        'units': 'metric',
      });
      print(url);
      var response = await http.get(url);
      if (response.statusCode == 200) {
        print('xxxx');
        var jsonResponse =
            convert.jsonDecode(response.body) as Map<String, dynamic>;
        modelWeatherForecast = ModelWeatherForecast.fromJson(jsonResponse);
        print('Request successful: $jsonResponse');
        return modelWeatherForecast;
      } else if (response.statusCode == 404) {
        throw 'City does not found, we\'ll update it soon';
      } else
        throw 'Request failed with status: ${response.statusCode}';
    } on SocketException {
      throw 'No internet connection 😑';
    } on HttpException {
      throw 'Couldn\'t find the post 😱';
    } on FormatException {
      throw 'Bad response format 👎';
    }
  }
}
