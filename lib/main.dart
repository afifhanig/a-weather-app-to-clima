import 'package:flutter/material.dart';
import 'package:pretest_jds/page/page_landing.dart';
import 'package:pretest_jds/page/page_splashscreen.dart';
import 'package:get/get.dart';
import 'package:pretest_jds/page/page_weather.dart';
import 'package:pretest_jds/page/page_weather_forecast.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        getPages: [
          GetPage(name: '/splash', page: () => SplashScreen()),
          GetPage(name: '/landing', page: () => LandingPage()),
          GetPage(name: '/weather', page: () => WeatherPage()),
          GetPage(name: '/forecast', page: () => ForecastPage()),
        ],
        home: LandingPage());
  }
}
