import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pretest_jds/page/page_weather_forecast.dart';
import 'package:pretest_jds/page/page_weather_today.dart';
import 'package:pretest_jds/controller/appcontroller.dart';

class WeatherPage extends StatefulWidget {
  const WeatherPage({Key? key}) : super(key: key);

  @override
  State<WeatherPage> createState() => _WeatherPageState();
}

class _WeatherPageState extends State<WeatherPage> {
  PageController pageController = PageController(initialPage: 0);
  int? currentIndex;
  final controller = Get.put(AppController());

  String _funcBGCustomy() {
    var hour = DateTime.now().hour;
    if (hour < 11) {
      if (controller.listIconToday[0].main == "Rain") {
        return 'assets/images/morning_rain.png';
      } else
        return 'assets/images/morning_clear.png';
    }
    if (hour < 15) {
      if (controller.listIconToday[0].main == "Rain") {
        return 'assets/images/afternoon_rain.png';
      } else
        return 'assets/images/afternoon_clear.png';
    }
    if (hour < 18) {
      if (controller.listIconToday[0].main == "Rain") {
        return 'assets/images/evening_rain.png';
      } else
        return 'assets/images/evening_clear.png';
    } else {
      if (controller.listIconToday[0].main == "Rain") {
        return 'assets/images/night_rain.png';
      } else
        return 'assets/images/night_clear.png';
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xffc6eeff),
        body: Stack(
          //fit: StackFit.expand, // expand stack
          children: [
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  colorFilter: ColorFilter.mode(
                      Colors.black.withOpacity(0.1), BlendMode.darken),
                  image: AssetImage(_funcBGCustomy()),
                  fit: BoxFit.cover,
                ),
              ),
              //Blur Widget
              child: currentIndex == 1
                  ? BackdropFilter(
                      filter: ImageFilter.blur(sigmaX: 1.5, sigmaY: 1.5),
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white.withOpacity(0.0),
                        ),
                      ),
                    )
                  : null,
            ),
            PageView(
              controller: pageController,
              children: [
                WeatherTodayPage(),
                ForecastPage(),
              ],
              onPageChanged: (int page) {
                setState(() {
                  currentIndex = page;
                });
                //print('xx ' + currentIndex.toString());
              },
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    print("disposed");
  }
}
