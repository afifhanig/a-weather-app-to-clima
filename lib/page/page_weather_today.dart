import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:pretest_jds/controller/appcontroller.dart';
import 'package:google_fonts/google_fonts.dart';

class WeatherTodayPage extends StatelessWidget {
  WeatherTodayPage({Key? key}) : super(key: key);
  final controller = Get.put(AppController());
  final now = DateTime.now();
  final name = Get.arguments;

  String _funcFormatHours(int index, DateTime date) {
    String formattedTime = DateFormat.jm().format(date);
    return formattedTime;
  }

  String _funcFormatDate(DateTime date) {
    String formattedDate = DateFormat.yMMMMd().format(date);
    //print(formattedDate);
    return formattedDate;
  }

  String _funcGreeting() {
    var hour = DateTime.now().hour;
    if (hour < 11) {
      return 'Good Morning';
    }
    if (hour < 15) {
      return 'Good Afternoon';
    }
    return 'Good Evening';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        children: [
          Positioned(
            top: 35,
            left: 20,
            //right: 0,
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(_funcFormatDate(now),
                      style: GoogleFonts.comicNeue(
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                        color: Colors.white,
                      )),
                  Text('${_funcGreeting()}, ',
                      style: GoogleFonts.rancho(
                        fontSize: 40,
                        fontWeight: FontWeight.w600,
                        color: Colors.white,
                      )),
                  name != null
                      ? Text(
                          name,
                          style: GoogleFonts.comicNeue(
                            fontSize: 16,
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                          ),
                        )
                      : Text(
                          'You',
                          style: GoogleFonts.comicNeue(
                            fontSize: 16,
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                ],
              ),
            ),
          ),
          Positioned(
            bottom: 20,
            left: 0,
            right: 0,
            //right: 0,
            child: Container(
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Icon(
                                  Icons.location_on,
                                  color: Colors.red,
                                ),
                                Text(
                                  controller.weatherToday!.name!, //'Bandung',
                                  style: GoogleFonts.comicNeue(
                                    fontSize: 16,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ],
                            ),
                            Text(
                                controller.weatherToday!.main!.temp.toString() +
                                    '\u2103',
                                style: GoogleFonts.comicNeue(
                                  fontSize: 40,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.white,
                                )),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(right: 20.0),
                        child: Column(
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                    controller
                                        .listIconToday[0].main!, //'Clear',
                                    style: GoogleFonts.comicNeue(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.white,
                                    )),
                                Container(
                                  width: 50,
                                  height: 50,
                                  //color: Colors.red,
                                  child: Align(
                                    alignment: Alignment.bottomCenter,
                                    child: Image.network(
                                        "http://openweathermap.org/img/w/" +
                                            controller.listIconToday[0].icon! +
                                            ".png"),
                                  ),
                                ),
                              ],
                            ),
                            // Text(
                            //   'Humidity ' +
                            //       controller.weatherToday.main.humidity
                            //           .toString() +
                            //       '%',
                            //   style: GoogleFonts.comicNeue(
                            //     color: Colors.white,
                            //     fontSize: 16,
                            //   ),
                            //   overflow: TextOverflow.ellipsis,
                            // ),
                            // Text(
                            //   'Wind ' +
                            //       controller.weatherToday.wind.speed
                            //           .toString() +
                            //       ' m/s',
                            //   style: GoogleFonts.comicNeue(
                            //     color: Colors.white,
                            //     fontSize: 16,
                            //   ),
                            //   overflow: TextOverflow.ellipsis,
                            // ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 25.0),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Hourly',
                        style: GoogleFonts.comicNeue(
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                  Obx(() {
                    return Container(
                      height: 100,
                      child: ListView.builder(
                        padding: EdgeInsets.only(left: 20),
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemCount: controller.listWeatherForecastToday.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            width: 100,
                            child: Card(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0)),
                              color: Colors.transparent,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    height: 50,
                                    child: Image.network(
                                        "http://openweathermap.org/img/w/" +
                                            controller
                                                .listWeatherDetailForecastToday[
                                                    index]
                                                .icon! +
                                            ".png"),
                                  ),
                                  Text(
                                    controller
                                        .listWeatherDetailForecastToday[index]
                                        .main!,
                                    style: GoogleFonts.comicNeue(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                  Text(
                                    _funcFormatHours(
                                        index,
                                        controller
                                            .listWeatherForecastToday[index]
                                            .dtTxt!),
                                    style: GoogleFonts.comicNeue(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    );
                  }),
                  // Container(
                  //   height: 60,
                  //   child: ListView(
                  //     padding: EdgeInsets.only(left: 20),
                  //     scrollDirection: Axis.horizontal,
                  //     children: [
                  //       Container(
                  //         width: 90,
                  //         child: Card(
                  //           color: Colors.transparent,
                  //           child: Padding(
                  //             padding: const EdgeInsets.all(4.0),
                  //             child: Column(
                  //               crossAxisAlignment: CrossAxisAlignment.center,
                  //               mainAxisAlignment: MainAxisAlignment.center,
                  //               children: [
                  //                 Text(
                  //                   controller.weatherToday.main.humidity
                  //                           .toString() +
                  //                       '%', //'86%',
                  //                   style: GoogleFonts.comicNeue(
                  //                     color: Colors.white,
                  //                   ),
                  //                 ),
                  //                 Text(
                  //                   'Humidity',
                  //                   style: GoogleFonts.comicNeue(
                  //                     color: Colors.white,
                  //                   ),
                  //                   overflow: TextOverflow.ellipsis,
                  //                 ),
                  //               ],
                  //             ),
                  //           ),
                  //         ),
                  //       ),
                  //       Container(
                  //         width: 90,
                  //         child: Card(
                  //           color: Colors.transparent,
                  //           child: Padding(
                  //             padding: const EdgeInsets.all(4.0),
                  //             child: Column(
                  //               crossAxisAlignment: CrossAxisAlignment.center,
                  //               mainAxisAlignment: MainAxisAlignment.center,
                  //               children: [
                  //                 Text(
                  //                   controller.weatherToday.main.pressure
                  //                           .toString() +
                  //                       ' hpa',
                  //                   style: GoogleFonts.comicNeue(
                  //                     color: Colors.white,
                  //                   ),
                  //                   overflow: TextOverflow.ellipsis,
                  //                 ),
                  //                 Text(
                  //                   'Pressure',
                  //                   style: GoogleFonts.comicNeue(
                  //                     color: Colors.white,
                  //                   ),
                  //                   overflow: TextOverflow.ellipsis,
                  //                 ),
                  //               ],
                  //             ),
                  //           ),
                  //         ),
                  //       ),
                  //       Container(
                  //         width: 90,
                  //         child: Card(
                  //           color: Colors.transparent,
                  //           child: Padding(
                  //             padding: const EdgeInsets.all(4.0),
                  //             child: Column(
                  //               crossAxisAlignment: CrossAxisAlignment.center,
                  //               mainAxisAlignment: MainAxisAlignment.center,
                  //               children: [
                  //                 Text(
                  //                   controller.weatherToday.clouds.all
                  //                           .toString() +
                  //                       '%',
                  //                   style: GoogleFonts.comicNeue(
                  //                     color: Colors.white,
                  //                   ),
                  //                 ),
                  //                 Text(
                  //                   'Cloudiness',
                  //                   style: GoogleFonts.comicNeue(
                  //                     color: Colors.white,
                  //                   ),
                  //                   overflow: TextOverflow.ellipsis,
                  //                 ),
                  //               ],
                  //             ),
                  //           ),
                  //         ),
                  //       ),
                  //       Container(
                  //         width: 90,
                  //         child: Card(
                  //           color: Colors.transparent,
                  //           child: Padding(
                  //             padding: const EdgeInsets.all(4.0),
                  //             child: Column(
                  //               crossAxisAlignment: CrossAxisAlignment.center,
                  //               mainAxisAlignment: MainAxisAlignment.center,
                  //               children: [
                  //                 Text(
                  //                   controller.weatherToday.wind.speed
                  //                           .toString() +
                  //                       ' m/s',
                  //                   style: GoogleFonts.comicNeue(
                  //                     color: Colors.white,
                  //                   ),
                  //                   overflow: TextOverflow.ellipsis,
                  //                 ),
                  //                 Text(
                  //                   'Wind',
                  //                   style: GoogleFonts.comicNeue(
                  //                     color: Colors.white,
                  //                   ),
                  //                   overflow: TextOverflow.ellipsis,
                  //                 ),
                  //               ],
                  //             ),
                  //           ),
                  //         ),
                  //       ),
                  //     ],
                  //   ),
                  // ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(
                          top: 10.0,
                          right: 20.0,
                        ),
                        child: GestureDetector(
                          onTap: () {},
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                'Next five days (Swipe)',
                                style: GoogleFonts.comicNeue(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.white,
                                ),
                              ),
                              Icon(
                                Icons.keyboard_double_arrow_right_rounded,
                                color: Colors.white,
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
