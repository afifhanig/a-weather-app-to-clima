import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pretest_jds/controller/appcontroller.dart';
import 'package:intl/intl.dart';
import 'package:google_fonts/google_fonts.dart';

class ForecastPage extends StatefulWidget {
  const ForecastPage({Key? key}) : super(key: key);

  @override
  State<ForecastPage> createState() => _ForecastPageState();
}

class _ForecastPageState extends State<ForecastPage> {
  final now = DateTime.now();
  final controller = Get.put(AppController());
  PageController pageController = PageController(initialPage: 0);

  _funcFormatHours(int index, DateTime date) {
    String formattedTime = DateFormat.jm().format(date);
    return formattedTime;
  }

  _funcFormatDate(DateTime date) {
    String formattedDate = DateFormat.yMMMMd().format(date);
    //print(formattedDate);
    return formattedDate;
  }

  // _funcFormatDay(int index, DateTime date) {
  //   String formattedDay = DateFormat.EEEE().format(date);
  //   //print(formattedDay);
  //   return formattedDay;
  // }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(top: 20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 10.0, bottom: 10.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.keyboard_double_arrow_left_rounded,
                        color: Colors.white,
                      ),
                      Text(
                        'Today weather (Swipe)',
                        style: GoogleFonts.comicNeue(
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),

                ///
                Padding(
                  padding: const EdgeInsets.only(left: 20.0),
                  child: Container(
                    padding: EdgeInsets.only(left: 15.0),
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(5.0),
                        bottomLeft: Radius.circular(5.0),
                      ),
                      color: Color(0xfffbf3bb),
                    ),
                    child: Text(
                      _funcFormatDate(
                          controller.listWeatherForecastDay1[0].dtTxt!),
                      style: GoogleFonts.comicNeue(
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff3d6a75),
                      ),
                    ),
                  ),
                ),
                Obx(() {
                  //List Weather Forecast Day 1
                  return Container(
                    height: 100,
                    child: ListView.builder(
                        padding: EdgeInsets.only(
                          left: 15.0,
                        ),
                        scrollDirection: Axis.horizontal,
                        itemCount: controller.listWeatherForecastDay1.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            width: 100,
                            child: Card(
                              color: Colors.transparent,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(
                                  10.0,
                                ),
                              ),
                              elevation: 3,
                              child: Container(
                                padding: EdgeInsets.all(10.0),
                                child: Column(
                                  children: [
                                    // Expanded(
                                    //   child: Text(
                                    //     _funcFormatDate(
                                    //         index,
                                    //         controller
                                    //             .listWeatherForecastDay1[index]
                                    //             .dtTxt!),
                                    //   ),
                                    // ),
                                    Expanded(
                                      child: Container(
                                        child: Image.network(
                                            "http://openweathermap.org/img/w/" +
                                                controller
                                                    .listWeatherDetailForecastD1[
                                                        index]
                                                    .icon! +
                                                ".png"),
                                      ),
                                    ),
                                    Text(
                                      controller
                                          .listWeatherDetailForecastD1[index]
                                          .main!,
                                      style: GoogleFonts.comicNeue(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                    // Expanded(
                                    //   child: Text(
                                    //     _funcFormatDay(
                                    //         index,
                                    //         controller
                                    //             .listWeatherForecastDay1[index]
                                    //             .dtTxt),
                                    //   ),
                                    // ),
                                    Text(
                                      _funcFormatHours(
                                          index,
                                          controller
                                              .listWeatherForecastDay1[index]
                                              .dtTxt!),
                                      style: GoogleFonts.comicNeue(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        }),
                  );
                }),

                ///
                SizedBox(height: 10.0),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0),
                  child: Container(
                    padding: EdgeInsets.only(left: 15.0),
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(5.0),
                        bottomLeft: Radius.circular(5.0),
                      ),
                      color: Color(0xfffbf3bb),
                    ),
                    child: Text(
                      _funcFormatDate(
                          controller.listWeatherForecastDay2[0].dtTxt!),
                      style: GoogleFonts.comicNeue(
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff3d6a75),
                      ),
                    ),
                  ),
                ),
                Obx(() {
                  //List Weather Forecast Day 2
                  return Container(
                    height: 100,
                    child: ListView.builder(
                        padding: EdgeInsets.only(
                          left: 15.0,
                        ),
                        scrollDirection: Axis.horizontal,
                        itemCount: controller.listWeatherForecastDay2.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            width: 100,
                            child: Card(
                              color: Colors.transparent,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(
                                  10.0,
                                ),
                              ),
                              elevation: 3,
                              child: Container(
                                padding: EdgeInsets.all(10.0),
                                child: Column(
                                  children: [
                                    // Expanded(
                                    //   child: Text(
                                    //     _funcFormatDate(
                                    //         index,
                                    //         controller
                                    //             .listWeatherForecastDay2[index]
                                    //             .dtTxt),
                                    //   ),
                                    // ),
                                    Expanded(
                                      child: Container(
                                        child: Image.network(
                                            "http://openweathermap.org/img/w/" +
                                                controller
                                                    .listWeatherDetailForecastD2[
                                                        index]
                                                    .icon! +
                                                ".png"),
                                      ),
                                    ),
                                    Text(
                                      controller
                                          .listWeatherDetailForecastD2[index]
                                          .main!,
                                      style: GoogleFonts.comicNeue(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                    // Expanded(
                                    //   child: Text(
                                    //     _funcFormatDay(
                                    //         index,
                                    //         controller
                                    //             .listWeatherForecastDay2[index]
                                    //             .dtTxt),
                                    //   ),
                                    // ),
                                    Text(
                                      _funcFormatHours(
                                          index,
                                          controller
                                              .listWeatherForecastDay2[index]
                                              .dtTxt!),
                                      style: GoogleFonts.comicNeue(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        }),
                  );
                }),

                ///
                SizedBox(height: 10.0),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0),
                  child: Container(
                    padding: EdgeInsets.only(left: 15.0),
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(5.0),
                        bottomLeft: Radius.circular(5.0),
                      ),
                      color: Color(0xfffbf3bb),
                    ),
                    child: Text(
                      _funcFormatDate(
                          controller.listWeatherForecastDay3[0].dtTxt!),
                      style: GoogleFonts.comicNeue(
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff3d6a75),
                      ),
                    ),
                  ),
                ),
                Obx(() {
                  //List Weather Forecast Day 3
                  return Container(
                    height: 100,
                    child: ListView.builder(
                        padding: EdgeInsets.only(
                          left: 15.0,
                        ),
                        scrollDirection: Axis.horizontal,
                        itemCount: controller.listWeatherForecastDay3.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            width: 100,
                            child: Card(
                              color: Colors.transparent,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(
                                  10.0,
                                ),
                              ),
                              elevation: 3,
                              child: Container(
                                padding: EdgeInsets.all(10.0),
                                child: Column(
                                  children: [
                                    // Expanded(
                                    //   child: Text(
                                    //     _funcFormatDate(
                                    //         index,
                                    //         controller
                                    //             .listWeatherForecastDay3[index]
                                    //             .dtTxt),
                                    //   ),
                                    // ),
                                    Expanded(
                                      child: Container(
                                        child: Image.network(
                                            "http://openweathermap.org/img/w/" +
                                                controller
                                                    .listWeatherDetailForecastD3[
                                                        index]
                                                    .icon! +
                                                ".png"),
                                      ),
                                    ),
                                    Text(
                                      controller
                                          .listWeatherDetailForecastD3[index]
                                          .main!,
                                      style: GoogleFonts.comicNeue(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                    // Expanded(
                                    //   child: Text(
                                    //     _funcFormatDay(
                                    //         index,
                                    //         controller
                                    //             .listWeatherForecastDay3[index]
                                    //             .dtTxt),
                                    //   ),
                                    // ),
                                    Text(
                                      _funcFormatHours(
                                          index,
                                          controller
                                              .listWeatherForecastDay3[index]
                                              .dtTxt!),
                                      style: GoogleFonts.comicNeue(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        }),
                  );
                }),

                ///
                SizedBox(height: 10.0),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0),
                  child: Container(
                    padding: EdgeInsets.only(left: 15.0),
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(5.0),
                        bottomLeft: Radius.circular(5.0),
                      ),
                      color: Color(0xfffbf3bb),
                    ),
                    child: Text(
                      _funcFormatDate(
                          controller.listWeatherForecastDay4[0].dtTxt!),
                      style: GoogleFonts.comicNeue(
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff3d6a75),
                      ),
                    ),
                  ),
                ),
                Obx(() {
                  //List Weather Forecast Day 4
                  return Container(
                    height: 100,
                    child: ListView.builder(
                        padding: EdgeInsets.only(
                          left: 15.0,
                        ),
                        scrollDirection: Axis.horizontal,
                        itemCount: controller.listWeatherForecastDay4.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            width: 100,
                            child: Card(
                              color: Colors.transparent,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(
                                  10.0,
                                ),
                              ),
                              elevation: 3,
                              child: Container(
                                padding: EdgeInsets.all(10.0),
                                child: Column(
                                  children: [
                                    // Expanded(
                                    //   child: Text(
                                    //     _funcFormatDate(
                                    //         index,
                                    //         controller
                                    //             .listWeatherForecastDay4[index]
                                    //             .dtTxt),
                                    //   ),
                                    // ),
                                    Expanded(
                                      child: Container(
                                        child: Image.network(
                                            "http://openweathermap.org/img/w/" +
                                                controller
                                                    .listWeatherDetailForecastD4[
                                                        index]
                                                    .icon! +
                                                ".png"),
                                      ),
                                    ),
                                    Text(
                                      controller
                                          .listWeatherDetailForecastD4[index]
                                          .main!,
                                      style: GoogleFonts.comicNeue(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),

                                    // Expanded(
                                    //   child: Text(
                                    //     _funcFormatDay(
                                    //         index,
                                    //         controller
                                    //             .listWeatherForecastDay4[index]
                                    //             .dtTxt),
                                    //   ),
                                    // ),
                                    Text(
                                      _funcFormatHours(
                                          index,
                                          controller
                                              .listWeatherForecastDay4[index]
                                              .dtTxt!),
                                      style: GoogleFonts.comicNeue(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        }),
                  );
                }),

                ///
                SizedBox(height: 10.0),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0),
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(5.0),
                        bottomLeft: Radius.circular(5.0),
                      ),
                      color: Color(0xfffbf3bb),
                    ),
                    padding: EdgeInsets.only(left: 15.0),
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      _funcFormatDate(
                          controller.listWeatherForecastDay5[0].dtTxt!),
                      style: GoogleFonts.comicNeue(
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff3d6a75),
                      ),
                    ),
                  ),
                ),
                Obx(() {
                  //List Weather Forecast Day 5
                  return Container(
                    height: 100,
                    child: ListView.builder(
                        padding: EdgeInsets.only(
                          left: 15.0,
                        ),
                        scrollDirection: Axis.horizontal,
                        itemCount: controller.listWeatherForecastDay5.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            width: 100,
                            child: Card(
                              color: Colors.transparent,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(
                                  10.0,
                                ),
                              ),
                              elevation: 3,
                              child: Container(
                                padding: EdgeInsets.all(10.0),
                                child: Column(
                                  children: [
                                    // Expanded(
                                    //   child: Text(
                                    //     _funcFormatDate(
                                    //         index,
                                    //         controller
                                    //             .listWeatherForecastDay5[index]
                                    //             .dtTxt),
                                    //   ),
                                    // ),
                                    Expanded(
                                      child: Container(
                                        child: Image.network(
                                            "http://openweathermap.org/img/w/" +
                                                controller
                                                    .listWeatherDetailForecastD5[
                                                        index]
                                                    .icon! +
                                                ".png"),
                                      ),
                                    ),
                                    Text(
                                      controller
                                          .listWeatherDetailForecastD5[index]
                                          .main!,
                                      style: GoogleFonts.comicNeue(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                    // Expanded(
                                    //   child: Text(
                                    //     _funcFormatDay(
                                    //         index,
                                    //         controller
                                    //             .listWeatherForecastDay5[index]
                                    //             .dtTxt),
                                    //   ),
                                    // ),
                                    Text(
                                      _funcFormatHours(
                                          index,
                                          controller
                                              .listWeatherForecastDay5[index]
                                              .dtTxt!),
                                      style: GoogleFonts.comicNeue(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        }),
                  );
                }),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
