import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pretest_jds/controller/appcontroller.dart';
import 'package:pretest_jds/custom_widget/custom_progress_indicator.dart';
import 'package:google_fonts/google_fonts.dart';

class LandingPage extends StatefulWidget {
  const LandingPage({Key? key}) : super(key: key);

  @override
  State<LandingPage> createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  final controller = Get.put(AppController());
  String? provinceValue;
  String? cityValue;
  TextEditingController nameController = TextEditingController();
  bool isLoading = false;

  @override
  void initState() {
    getProvince();
    super.initState();
  }

  void getProvince() async {
    await controller.getProvince();
    if (controller.errorMessage.isNotEmpty) {
      showSnackBar(context, controller.errorMessage.value);
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        //backgroundColor: Color(0xfffcf1b9),
        resizeToAvoidBottomInset: false,
        body: Stack(
          children: [
            Image.asset(
              "assets/images/115.png",
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              fit: BoxFit.cover,
            ),
            Center(child: mainWidget()),
            isLoading ? CustomProgressIndicatorWidget() : Container(),
            Align(
                alignment: Alignment.bottomCenter,
                child: Text(
                  '\u00a9 2022 Afif Hani G',
                  style: GoogleFonts.comicNeue(
                    fontSize: 15,
                    fontWeight: FontWeight.w600,
                  ),
                )),
          ],
        ),
      ),
    );
  }

  Widget mainWidget() {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.all(15.0),
        //height: MediaQuery.of(context).size.height,
        child: Column(
          children: [
            Container(
              height: 150,
              child: Image.asset('assets/images/toclima_logo.png'),
            ),
            // Text(
            //   'to Clima',
            //   style: GoogleFonts.rancho(
            //     fontSize: 70,
            //     fontWeight: FontWeight.w600,
            //     color: Color(0xff3d6a75),
            //   ),
            // ),
            SizedBox(height: 15),
            Center(
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  children: [
                    userName(),
                    SizedBox(height: 10),
                    province(),
                    Obx(() {
                      return controller.isLoadingProvince.value
                          ? SizedBox(
                              width: 250,
                              child: LinearProgressIndicator(),
                            )
                          : Container();
                    }),
                    SizedBox(height: 10),
                    city(),
                    Obx(() {
                      return controller.isLoadingCity.value
                          ? SizedBox(
                              width: 250,
                              child: LinearProgressIndicator(),
                            )
                          : Container();
                    }),
                    SizedBox(height: 10),
                    disableButton() ? Container() : info(),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: buttonContinue(),
                    ),
                    //testingButton(),
                    //testingButton2(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget userName() {
    return Material(
      elevation: 7.0,
      shadowColor: Color(0xffed8220),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: TextFormField(
        style: GoogleFonts.comicNeue(
          fontSize: 16,
        ),
        textCapitalization: TextCapitalization.words,
        controller: nameController,
        decoration: InputDecoration(
          labelText: 'Name',
          labelStyle: GoogleFonts.comicNeue(
            fontSize: 16,
          ),
          // filled: true,
          // fillColor: Color(0xfffbc943),
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Color(0xff3d6a75),
              ),
              borderRadius: BorderRadius.circular(10)),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Color(0xff3d6a75),
            ),
            borderRadius: BorderRadius.circular(10),
          ),
          contentPadding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 10.0),
        ),
      ),
    );
  }

  Widget province() {
    return Obx(() {
      return Material(
        elevation: 7.0,
        shadowColor: Color(0xffed8220),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: DropdownButtonFormField<String>(
          //style: GoogleFonts.comicNeue(),
          decoration: InputDecoration(
            // filled: true,
            // fillColor: Color(0xfffbc943),
            labelText: 'Province',
            labelStyle: GoogleFonts.comicNeue(
              fontSize: 16,
            ),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Color(0xff3d6a75),
                ),
                borderRadius: BorderRadius.circular(10)),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Color(0xff3d6a75),
              ),
              borderRadius: BorderRadius.circular(10),
            ),
            contentPadding:
                EdgeInsets.symmetric(vertical: 0.0, horizontal: 10.0),
          ),
          isExpanded: true,
          items: controller.successGetProvince.value
              ? controller.listProvince.map((e) {
                  return DropdownMenuItem(
                    value: e.provinceId.toString(),
                    child: Text(e.province!),
                  );
                }).toList()
              : [],
          value: provinceValue,
          onChanged: (value) async {
            setState(() {
              provinceValue = value;
            });
            print(provinceValue);
            if (cityValue != null) {
              setState(() {
                cityValue = null;
              });
            }
            //set city success to false if it's true
            if (controller.successGetCity.value) {
              controller.successGetCity.value = false;
            }
            //clear city list if it's not empty
            if (controller.listCity.isNotEmpty) {
              controller.listCity.clear();
            }
            controller.setProvince(value.toString());
            await controller.getCity();
            if (controller.errorMessage.isNotEmpty) {
              showSnackBar(context, controller.errorMessage.value);
            }
          },
        ),
      );
    });
  }

  Widget city() {
    return Obx(() {
      return Material(
        elevation: 7.0,
        shadowColor: Color(0xffed8220),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: DropdownButtonFormField<String>(
          //style: GoogleFonts.comicNeue(),
          decoration: InputDecoration(
            labelText: 'City',
            labelStyle: GoogleFonts.comicNeue(
              fontSize: 16,
            ),
            // filled: true,
            // fillColor: Color(0xfffbc943),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Color(0xff3d6a75),
                ),
                borderRadius: BorderRadius.circular(10)),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Color(0xff3d6a75),
              ),
              borderRadius: BorderRadius.circular(10),
            ),
            contentPadding:
                EdgeInsets.symmetric(vertical: 0.0, horizontal: 10.0),
          ),
          isExpanded: true,
          items: controller.successGetCity.value
              ? controller.listCity.map((e) {
                  return DropdownMenuItem(
                    value: e.cityName.toString(),
                    child: Text(e.cityName!),
                  );
                }).toList()
              : [],
          value: cityValue,
          onChanged: (value) {
            setState(() {
              cityValue = value;
            });
            if (provinceValue == '6') {
              print('jakarta');
              controller.setCity("jakarta");
            } else {
              print('not jakarta');
              var x = cityValue!.replaceAll(' ', '');
              controller.setCity(x.toLowerCase());
            }
          },
        ),
      );
    });
  }

  Widget buttonContinue() {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        primary: Color(0xff6494a4),
        shadowColor: Color(0xff3d6a75),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
      ),
      onPressed: disableButton()
          ? () async {
              if (nameController.text != '' &&
                  provinceValue != null &&
                  cityValue != null) {
                //
                setState(() {
                  isLoading = true;
                });
                await controller.getWeatherToday();
                await controller.getWeatherForecast();
                if (controller.errorMessage.isNotEmpty) {
                  showSnackBar(context, controller.errorMessage.value);
                }
                setState(() {
                  isLoading = false;
                });

                //
                if (controller.successGetWeatherToday.value &&
                    controller.successGetWeatherForecast.value) {
                  Get.toNamed('/weather', arguments: nameController.text)!
                      .then((value) {
                    controller.clear();
                  });
                }
              } else {
                showSnackBar(context, 'Please fill all the form');
              }
            }
          : null,
      child: Text(
        'Continue',
        style: GoogleFonts.comicNeue(
          fontSize: 16,
          fontWeight: FontWeight.w600,
          color: Colors.white,
        ),
      ),
    );
  }

  showSnackBar(BuildContext context, String message) {
    Flushbar(
      backgroundColor: Colors.red,
      message: message,
      duration: Duration(milliseconds: 2500),
      flushbarStyle: FlushbarStyle.FLOATING,
      margin: EdgeInsets.all(8),
      borderRadius: BorderRadius.circular(10),
      icon: Icon(
        Icons.info_outline,
        size: 28.0,
        color: Colors.white,
      ),
    )..show(context).then(
        (value) => controller.errorMessage.value = '',
      );
    return SizedBox.shrink();
  }

  Widget info() {
    return Container(
      padding: EdgeInsets.all(5.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: Color(0xffed8220),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(
            Icons.info,
            size: 18,
            color: Colors.white,
          ),
          SizedBox(width: 8),
          cityValue == null
              ? Text(
                  'Fill the form to continue',
                  style: GoogleFonts.comicNeue(
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                  ),
                )
              : Text(
                  'You can continue',
                  style: GoogleFonts.comicNeue(
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                  ),
                ),
        ],
      ),
    );
  }

  Widget testingButton() {
    return RaisedButton(
      onPressed: () async {
        //showSnackBar(context, 'The message is clear');
        // print(provinceValue);
        // print(cityValue);
        //controller.getCity();
        // await controller.getWeatherToday();
        // if (controller.errorMessage.isNotEmpty) {
        //   showSnackBar(context, controller.errorMessage.value);
        // }
        //getProvince();
        //controller.getWeatherToday();
        controller.getWeatherForecast();
      },
      child: Text('Refresh'),
    );
  }

  bool disableButton() {
    if (cityValue != null) {
      return true;
    } else
      return false;
  }
}
